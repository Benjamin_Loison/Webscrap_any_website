import blackboxprotobuf
import base64
import time
import hashlib
from lxml import html
import json
import requests
from datetime import datetime
import re

ORIGIN = 'https://www.youtube.com'
# TODO: Simplify `USER_AGENT` and `CLIENT_VERSION` by testing YouTube operational API ones.
USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:130.0) Gecko/20100101 Firefox/130.0'
CLIENT_VERSION = '2.20240918.06.00'

def getBase64Protobuf(message, typedef):
    data = blackboxprotobuf.encode_message(message, typedef)
    return base64.b64encode(data).decode('ascii')

def getSAPISIDHASH(__Secure_1PAPISID):
    currentTime = int(time.time())
    SAPISIDHASH = f'{currentTime}_' + hashlib.sha1(f'{currentTime} {__Secure_1PAPISID} {ORIGIN}'.encode('ascii')).digest().hex()
    return SAPISIDHASH

def getYtInitialData(text):
    tree = html.fromstring(text)
    ytVariableDeclaration = 'ytInitialData = '
    for script in tree.xpath('//script'):
        scriptContent = script.text_content()
        if ytVariableDeclaration in scriptContent:
            newContent = scriptContent.split(ytVariableDeclaration)[1][:-1]
            break
    ytInitialData = json.loads(newContent)
    return ytInitialData

def getJSONFromHTML(url, params = {}, cookies = {}, headers = {}):
    text = requests.get(url, params, cookies = cookies, headers = headers).text
    ytInitialData = getYtInitialData(text)
    return ytInitialData

def getJSON(url, data):
    data['context'] = {
        'client': {
            'clientName': 'WEB',
            'clientVersion': CLIENT_VERSION,
        }
    }
    return requests.post(url, json = data).json()

# `cookies` must have entries:
# - `__Secure-1PSIDTS`
# - `__Secure-1PSID`
def getAuthenticatedChannelId(cookies):
    headers = {
        'User-Agent': USER_AGENT,
    }
    ytInitialData = getJSONFromHTML('https://www.youtube.com', cookies = cookies, headers = headers)
    # Could verify that looks like a channel id.
    channelId = ytInitialData['topbar']['desktopTopbarRenderer']['topbarButtons'][0]['buttonRenderer']['command']['openPopupAction']['popup']['multiPageMenuRenderer']['sections'][0]['multiPageMenuSectionRenderer']['items'][2]['compactLinkRenderer']['navigationEndpoint']['browseEndpoint']['browseId']
    return channelId

def getCookiesSubset(cookies, keys):
    return {cookie: cookies[cookie] for cookie in keys}

def getTab(ytInitialData, tabName):
    tabs = ytInitialData['contents']['twoColumnBrowseResultsRenderer']['tabs']
    for tab in tabs:
        tabTitle = tab['tabRenderer']['title']
        if tabTitle == tabName:
            return tab

def getIntFromDuration(timeStr):
    totalSeconds = sum(x * int(t) for x, t in zip([1, 60, 3_600, 24 * 3_600], reversed(timeStr.split(':'))))
    return totalSeconds

def getIntValue(valueStr, unitCount):
    valueStr = valueStr.replace(f' {unitCount}s', '')
    valueStr = valueStr.replace(',', '')
    amount = 1_000
    for unit in 'KMB':
        valueStr = valueStr.replace(unit, f'*{amount}')
        amount *= 1_000
    if re.fullmatch('[\\d.*KMB]+', valueStr):
        return int(eval(valueStr))

def getBrowse(continuation):
    data = {
        'context': {
            'client': {
                'clientName': 'WEB',
                'clientVersion': CLIENT_VERSION,
            },
        },
        'continuation': continuation,
    }
    browse = requests.post('https://www.youtube.com/youtubei/v1/browse', json = data).json()
    return browse

'''
`cookies` must have:
- `__Secure-1PSIDTS`
- `__Secure-1PSID`
- `__Secure-1PAPISID`
`authorization` must mention `SAPISIDHASH`, it is not mandatory to mention `SAPISID{1,3}PHASH`.
Current implementation is oriented on marking them as read, not retrieving them.
'''
def getNotifications(cookies, authorization):
    headers = {
        'Origin': 'https://www.youtube.com',
        'Authorization': authorization,
    }

    data = {
        'context': {
            'client': {
                'clientName': 'WEB',
                'clientVersion': CLIENT_VERSION,
            },
        },
        'browseId': 'FEnotifications_inbox',
    }

    data = requests.post('https://www.youtube.com/youtubei/v1/browse', cookies = cookies, headers = headers, json = data).json()
    return data
