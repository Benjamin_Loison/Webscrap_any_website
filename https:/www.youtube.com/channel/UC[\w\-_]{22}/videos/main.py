import requests
import json
import sys

sys.path.insert(0, '../../..')

from main import getBase64Protobuf, CLIENT_VERSION, getBrowse

# Similar to `getPopularShorts`
def getPopularVideos(channelId):
    message = {
        '110': {
            '3': {
                '15': {
                    '2': {
                        '1': '0'
                    },
                    '4': 2
                }
            }
        }
    }

    typedef = {
        '110': {
            'field_order': [
                '3'
            ],
            'message_typedef': {
                '3': {
                    'field_order': [
                        '15'
                    ],
                    'message_typedef': {
                        '15': {
                            'field_order': [
                                '2',
                                '4'
                            ],
                            'message_typedef': {
                                '2': {
                                    'field_order': [
                                        '1'
                                    ],
                                    'message_typedef': {
                                        '1': {
                                            'type': 'string'
                                        }
                                    },
                                    'type': 'message'
                                },
                                '4': {
                                    'type': 'int'
                                }
                            },
                            'type': 'message'
                        }
                    },
                    'type': 'message'
                }
            },
            'type': 'message'
        }
    }

    three = getBase64Protobuf(message, typedef)

    message = {
        '80226972': {
            '2': channelId,
            '3': three,
        }
    }

    typedef = {
        '80226972': {
            'field_order': [
                '2',
                '3'
            ],
            'message_typedef': {
                '2': {
                    'type': 'string'
                },
                '3': {
                    'type': 'string'
                }
            },
            'type': 'message'
        }
    }

    continuation = getBase64Protobuf(message, typedef)

    browse = getBrowse(continuation)
    videos = browse['onResponseReceivedActions'][1]['reloadContinuationItemsCommand']['continuationItems']
    def cleanVideo(video):
        try:
            videoRenderer = video['richItemRenderer']['content']['videoRenderer']
            # Incomplete
            return {
                'id': videoRenderer['videoId'],
                'thumbnails': videoRenderer['thumbnail']['thumbnails'],
                'title': videoRenderer['title']['runs'][0]['text'],
                'view count': int(videoRenderer['viewCountText']['simpleText'].replace(' views', '').replace(',', '')),
                'published at': videoRenderer['publishedTimeText']['simpleText'],
            }
        except KeyError:
            return
    videos = list(map(cleanVideo, videos))
    if videos[-1] is None:
        videos.pop()
    return videos
