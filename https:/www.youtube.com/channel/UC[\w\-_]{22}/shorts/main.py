import requests
import json
import sys

sys.path.insert(0, '../../..')

from main import getBase64Protobuf, CLIENT_VERSION, getIntValue, getBrowse

def getPopularShorts(channelId):
    message = {
        '110': {
            '3': {
                '10': {
                    '2': {
                        '1': '0'
                    },
                    '4': 2
                }
            }
        }
    }
    typedef = {
        '110': {
            'field_order': [
                '3'
            ],
            'message_typedef': {
                '3': {
                    'field_order': [
                        '10'
                    ],
                    'message_typedef': {
                        '10': {
                            'field_order': [
                                '2',
                                '4'
                            ],
                            'message_typedef': {
                                '2': {
                                    'field_order': [
                                        '1'
                                    ],
                                    'message_typedef': {
                                        '1': {
                                            'type': 'string'
                                        }
                                    },
                                    'type': 'message'
                                },
                                '4': {
                                    'type': 'int'
                                }
                            },
                            'type': 'message'
                        }
                    },
                    'type': 'message'
                }
            },
            'type': 'message'
        }
    }

    three = getBase64Protobuf(message, typedef)

    message = {
        '80226972': {
            '2': channelId,
            '3': three,
        }
    }

    typedef = {
        '80226972': {
            'field_order': [
                '2',
                '3'
            ],
            'message_typedef': {
                '2': {
                    'type': 'string'
                },
                '3': {
                    'type': 'string'
                }
            },
            'type': 'message'
        }
    }

    continuation = getBase64Protobuf(message, typedef)

    browse = getBrowse(continuation)
    videos = browse['onResponseReceivedActions'][1]['reloadContinuationItemsCommand']['continuationItems']
    def cleanVideo(video):
        try:
            shortsLockupViewModel = video['richItemRenderer']['content']['shortsLockupViewModel']
            # Incomplete
            return {
                'id': shortsLockupViewModel['onTap']['innertubeCommand']['reelWatchEndpoint']['videoId'],
                'title': shortsLockupViewModel['overlayMetadata']['primaryText']['content'],
                'thumbnail': shortsLockupViewModel['thumbnail']['sources'][0],
                'view count': getIntValue(shortsLockupViewModel['overlayMetadata']['secondaryText']['content'], 'view'),
            }
        except KeyError:
            return
    videos = list(map(cleanVideo, videos))
    if videos[-1] is None:
        videos.pop()
    return videos