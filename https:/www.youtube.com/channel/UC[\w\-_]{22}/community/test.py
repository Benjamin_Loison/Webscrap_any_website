from main import publishCommunityPost, getCommunityPosts
import sys

sys.path.insert(0, '../../..')

from main import getCookiesSubset, getAuthenticatedChannelId
from test import getRandomString

##

import time

COOKIES = {
    '__Secure-1PSIDTS': 'sidts-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
    '__Secure-1PSID': 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
    '__Secure-1PAPISID': 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
}

commentText = getRandomString()
print(f'{commentText=}')

state = publishCommunityPost(COOKIES, commentText)
print(f'{state=}')

getAuthenticatedChannelIdCookies = getCookiesSubset(COOKIES, ('__Secure-1PSIDTS', '__Secure-1PSID'))
channelId = getAuthenticatedChannelId(getAuthenticatedChannelIdCookies)
print(f'{channelId=}')

while True:
    communityPosts = getCommunityPosts(channelId)
    lastCommunityPostText = communityPosts[0]['text']
    isExpectedLastCommunityPostText = lastCommunityPostText == commentText
    print(f'{isExpectedLastCommunityPostText=}')
    if isExpectedLastCommunityPostText:
        break
    print('Sleeping 1 second...')
    time.sleep(1)

##

from random import randrange

commentText = getRandomString()
print(f'{commentText=}')

# 2 may not work on high latency network.
# Could be more precise concerning the upper bound.
timestamp = int(time.time()) + randrange(2, 365 * 24 * 3_600)
print(f'{timestamp=}')
state = publishCommunityPost(COOKIES, commentText, timestamp)
print(f'{state=}')

scheduledCommunityPosts = getScheduledCommunityPosts(COOKIES)
print(json.dumps(scheduledCommunityPosts, indent = 4))
lastScheduledCommunityPost = scheduledCommunityPosts[0]
print(lastScheduledCommunityPost['text'] == commentText and lastScheduledCommunityPost['scheduledPublishTimeSec'] == timestamp)

##

imagePaths = ('blue.png', 'red.png')
for numberOfImages in (1, 2):
    images = []
    for imagePath in imagePaths[:numberOfImages]:
        with open(imagePath, 'rb') as f:
            image = f.read()
            images += [image]

    commentText = getRandomString()
    print(f'{commentText=}')
    state = publishCommunityPost(COOKIES, commentText, images = images)
    print(f'{state=}')

    while True:
        communityPosts = getCommunityPosts(channelId)
        communityPost = communityPosts[0]
        if communityPost['text'] == commentText:
            print(len(communityPost['images']) == numberOfImages)
            break