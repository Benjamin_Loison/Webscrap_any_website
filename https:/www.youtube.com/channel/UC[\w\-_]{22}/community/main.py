import requests
import json
import sys

sys.path.insert(0, '../../..')

from main import getBase64Protobuf, getSAPISIDHASH, getAuthenticatedChannelId, getJSONFromHTML, getTab, ORIGIN, CLIENT_VERSION

HEADERS = {
    'Accept-Language': 'en',
}

def getCommunityPosts(channelId):
    ytInitialData = getJSONFromHTML(f'https://www.youtube.com/channel/{channelId}/community', headers = HEADERS)
    communityTab = getCommunityTab(ytInitialData)
    communityPosts = communityTab['tabRenderer']['content']['sectionListRenderer']['contents'][0]['itemSectionRenderer']['contents']
    def cleanCommunityPost(communityPost):
        try:
            # Incomplete
            backstagePostRenderer = communityPost['backstagePostThreadRenderer']['post']['backstagePostRenderer']
            images = []
            if 'backstageAttachment' in backstagePostRenderer:
                backstageAttachment = backstagePostRenderer['backstageAttachment']
                getThumbnails = lambda image: image['backstageImageRenderer']['image']['thumbnails'][0]
                if 'postMultiImageRenderer' in backstageAttachment:
                    images = list(map(getThumbnails, backstageAttachment['postMultiImageRenderer']['images']))
                else:
                    images = [getThumbnails(backstageAttachment)]
            contentText = backstagePostRenderer['contentText']
            return {
                'text': contentText['runs'][0]['text'] if 'runs' in contentText else None,
                'images': images,
            }
        except KeyError:
            return
    communityPosts = list(map(cleanCommunityPost, communityPosts))
    if communityPosts[-1] is None:
        communityPosts.pop()
    return communityPosts

# `cookies` must have entries:
# - `__Secure-1PSIDTS`
# - `__Secure-1PSID`
def getScheduledCommunityPosts(cookies):
    channelId = getAuthenticatedChannelId(cookies)

    MESSAGE = {
        '1': 1
    }

    TYPEDEF = {
        '1': {
            'type': 'int'
        }
    }

    PVF = getBase64Protobuf(MESSAGE, TYPEDEF)

    PARAMS = {
        'pvf': PVF,
    }

    COOKIES['PREF'] = 'hl=en'
    ytInitialData = getJSONFromHTML(f'https://www.youtube.com/channel/{channelId}/community', PARAMS, cookies, HEADERS)
    communityTab = getCommunityTab(ytInitialData)
    # If no community post are scheduled.
    try:
        scheduledCommunityPosts = communityTab['tabRenderer']['content']['sectionListRenderer']['contents'][0]['itemSectionRenderer']['contents']
    except KeyError:
        return []
    def cleanScheduledCommunityPost(scheduledCommunityPost):
        try:
            backstagePostRenderer = scheduledCommunityPost['backstagePostThreadRenderer']['post']['backstagePostRenderer']
            # Incomplete
            return {
                'id': backstagePostRenderer['postId'],
                'text': backstagePostRenderer['contentText']['runs'][0]['text'],
                'scheduled publish time sec': int(backstagePostRenderer['scheduledPublishTimeSec']),
            }
        except KeyError:
            return
    #return scheduledCommunityPosts
    scheduledCommunityPosts = list(map(cleanScheduledCommunityPost, scheduledCommunityPosts))
    if scheduledCommunityPosts[-1] is None:
        scheduledCommunityPosts.pop()
    return scheduledCommunityPosts

# `cookies` must have entries:
# - `__Secure-1PSIDTS`
# - `__Secure-1PSID`
# - `__Secure-1PAPISID`
def communityPostAction(cookies, url, data):
    SAPISIDHASH = getSAPISIDHASH(cookies['__Secure-1PAPISID'])

    headers = {
        'Authorization': f'SAPISIDHASH {SAPISIDHASH}',
        'Origin': ORIGIN,
    }

    data = data | {
        'context': {
            'client': {
                'clientName': 'WEB',
                'clientVersion': CLIENT_VERSION,
            },
        }
    }

    response = requests.post(
        url,
        cookies = cookies,
        headers = headers,
        json = data,
    )
    data = response.json()
    return data

# `cookies` must have entries:
# - `__Secure-1PSIDTS`
# - `__Secure-1PSID`
# - `__Secure-1PAPISID`
# `images` have to be provided as `bytes`.
def publishCommunityPost(cookies, commentText = '', scheduledPublishTimeSec = None, images = []):
    getAuthenticatedChannelIdCookies = getCookiesSubset(cookies, ('__Secure-1PSIDTS', '__Secure-1PSID'))
    channelId = getAuthenticatedChannelId(getAuthenticatedChannelIdCookies)

    message = {
        '1': channelId,
    }

    TYPEDEF = {
        '1': {
            'type': 'string'
        },
    }

    createBackstagePostParams = getBase64Protobuf(message, TYPEDEF)

    data = {
        'createBackstagePostParams': createBackstagePostParams,
    }

    if commentText != '':
        data['commentText'] = commentText

    if scheduledPublishTimeSec is not None:
        data['scheduledPublishTimeSec'] = scheduledPublishTimeSec

    if images != []:
        imagesData = []
        FIRST_HEADERS = {
            'X-YouTube-ChannelId': channelId,
            'X-Goog-Upload-Protocol': 'resumable',
            'X-Goog-Upload-Command': 'start',
        }
        URL = 'https://www.youtube.com/channel_image_upload/posts'
        SECOND_HEADERS = {
            'X-Goog-Upload-Command': 'upload, finalize',
            'X-Goog-Upload-Offset': '0',
        }
        params = {
            'upload_protocol': 'resumable',
        }
        for image in images:
            response = requests.post(URL, cookies = getAuthenticatedChannelIdCookies, headers = FIRST_HEADERS)
            params['upload_id'] = response.headers['X-GUploader-UploadID']

            response = requests.post(URL, image, params = params, headers = SECOND_HEADERS)

            imageData = {
                'encryptedBlobId': response.json()['encryptedBlobId'],
            }
            if len(images) > 1:
                imageData['previewCoordinates'] = {}
            imagesData += [imageData]

        data['imagesAttachment'] = {
            'imagesData': imagesData
        }

    data = communityPostAction(cookies, 'https://www.youtube.com/youtubei/v1/backstage/create_post', data)
    state = data['actions'][1]['openPopupAction']['popup']['notificationActionRenderer']['responseText']['runs'][0]['text']
    postSuccessfullyPublished = (state == 'Post created') if scheduledPublishTimeSec is None else (state == 'Your post has been scheduled')
    return postSuccessfullyPublished

# `cookies` must have entries:
# - `__Secure-1PSIDTS`
# - `__Secure-1PSID`
# - `__Secure-1PAPISID`
def deleteScheduledCommunityPost(cookies, communityPostId):
    getAuthenticatedChannelIdCookies = getCookiesSubset(cookies, ('__Secure-1PSIDTS', '__Secure-1PSID'))
    channelId = getAuthenticatedChannelId(getAuthenticatedChannelIdCookies)

    message = {
        '1': 6,
        '3': communityPostId,
        '11': channelId
    }

    TYPEDEF = {
        '1': {
            'type': 'int'
        },
        '3': {
            'type': 'string'
        },
        '11': {
            'type': 'string'
        }
    }

    actions = getBase64Protobuf(message, TYPEDEF)

    data = {
        'actions': actions,
    }

    data = communityPostAction(cookies, 'https://www.youtube.com/youtubei/v1/comment/perform_comment_action', data)
    state = data['actions'][0]['removeCommentAction']['actionResult']['feedbackText']['runs'][0]['text'] == 'Post deleted as '
    return state

def getCommunityTab(ytInitialData):
    return getTab(ytInitialData, 'Community')