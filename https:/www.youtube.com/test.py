import random
import string

def getRandomString(stringLength = 32):
   characters = string.ascii_letters + string.digits
   return ''.join(random.choice(characters) for characterIndex in range(stringLength))