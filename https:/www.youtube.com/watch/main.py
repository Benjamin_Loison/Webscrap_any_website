import requests
import sys

sys.path.insert(0, '../../..')

from main import getBase64Protobuf, CLIENT_VERSION

def getCommentCount(videoId):
    message = {
        '2': {
            '2': videoId
        },
        '3': 6,
        '6': {
            '4': {
                '4': videoId
            }
        }
    }

    typedef = {
        '2': {
            'field_order': [
                '2'
            ],
            'message_typedef': {
                '2': {
                    'type': 'string'
                }
            },
            'type': 'message'
        },
        '3': {
            'type': 'int'
        },
        '6': {
            'message_typedef': {
                '4': {
                    'message_typedef': {
                        '4': {
                            'type': 'string'
                        }
                    },
                    'type': 'message'
                },
            },
            'type': 'message'
        }
    }

    continuation = getBase64Protobuf(message, typedef)

    data = {
        'context': {
            'client': {
                'clientName': 'WEB',
                'clientVersion': CLIENT_VERSION,
            },
        },
        'continuation': continuation,
    }

    data = requests.post('https://www.youtube.com/youtubei/v1/next', json = data).json()
    return int(data['onResponseReceivedEndpoints'][0]['reloadContinuationItemsCommand']['continuationItems'][0]['commentsHeaderRenderer']['countText']['runs'][0]['text'].replace(',', ''))