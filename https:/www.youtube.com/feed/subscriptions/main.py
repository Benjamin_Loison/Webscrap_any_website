import sys

sys.path.insert(0, '../..')

from main import getJSONFromHTML

# `cookies` must have entries:
# - `__Secure-1PSIDTS`
# - `__Secure-1PSID`
# - `LOGIN_INFO` without it returned results are not exhaustive, see [Webscrap_any_website/issues/73](https://codeberg.org/Benjamin_Loison/Webscrap_any_website/issues/73)
def getVideosAndShorts(cookies):
    ytInitialData = getJSONFromHTML('https://www.youtube.com/feed/subscriptions', cookies = cookies)
    contents = ytInitialData['contents']['twoColumnBrowseResultsRenderer']['tabs'][0]['tabRenderer']['content']['richGridRenderer']['contents']
    videos = []
    for content in contents:
        # Not exhaustive
        # Is this condition really necessary?
        if 'richSectionRenderer' in content:
            try:
                shorts = content['richSectionRenderer']['content']['richShelfRenderer']['contents']
                def cleanShorts(short):
                    shortsLockupViewModel = short['richItemRenderer']['content']['shortsLockupViewModel']
                    return {
                        'id': shortsLockupViewModel['onTap']['innertubeCommand']['reelWatchEndpoint']['videoId'],
                        'title': shortsLockupViewModel['overlayMetadata']['primaryText']['content'],
                    }
                shorts = list(map(cleanShorts, shorts))
            except KeyError:
                continue
        else:
            try:
                videoRenderer = content['richItemRenderer']['content']['videoRenderer']
                thumbnailOverlay = videoRenderer['thumbnailOverlays'][0]
                videos += [{
                    'id': videoRenderer['videoId'],
                    'title': videoRenderer['title']['runs'][0]['text'],
                    'channel handle': videoRenderer['ownerText']['runs'][0]['navigationEndpoint']['browseEndpoint']['canonicalBaseUrl'][1:],
                    'channel title': videoRenderer['ownerText']['runs'][0]['text'],
                    'description': videoRenderer['descriptionSnippet']['runs'][0]['text'] if 'descriptionSnippet' in videoRenderer else None,
                    'percent watched': thumbnailOverlay['thumbnailOverlayResumePlaybackRenderer']['percentDurationWatched'] if 'thumbnailOverlayResumePlaybackRenderer' in thumbnailOverlay else 0,
                }]
            except KeyError:
                continue
    return videos, shorts