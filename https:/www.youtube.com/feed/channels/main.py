import sys

sys.path.insert(0, '../..')

from main import getJSONFromHTML

# `cookies` must have entries:
# - `__Secure-1PSIDTS`
# - `__Secure-1PSID`
# - `LOGIN_INFO`
def getSubscriptions(cookies):
    ytInitialData = getJSONFromHTML('https://www.youtube.com/feed/channels', cookies = cookies)
    subscriptions = ytInitialData['contents']['twoColumnBrowseResultsRenderer']['tabs'][0]['tabRenderer']['content']['sectionListRenderer']['contents'][0]['itemSectionRenderer']['contents'][0]['shelfRenderer']['content']['expandedShelfContentsRenderer']['items']
    def cleanSubscription(subscription):
        # Not exhaustive
        channelRenderer = subscription['channelRenderer']
        return {
            'id': channelRenderer['channelId'],
            'handle': channelRenderer['subscriberCountText']['simpleText'],
        }
    subscriptions = list(map(cleanSubscription, subscriptions))
    return subscriptions