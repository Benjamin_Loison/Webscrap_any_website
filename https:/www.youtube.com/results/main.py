import requests
from enum import Enum
import sys

sys.path.insert(0, '../')

from main import getBase64Protobuf, getJSON, getIntFromDuration

class Type(Enum):
    VIDEO = 1
    CHANNEL = 2
    PLAYLIST = 3

class SortBy(Enum):
    # *0* but let's keep *None* to mean default.
    # Let us keep webpage order.
    RELEVANCE = None
    UPLOAD_DATE = 2
    VIEW_COUNT = 3
    RATING = 1

def requiresToSendPage(page):
    return page is not None and page != 0

def search(query, type_, sortBy, page):
    message = {
        '2': {
            '2': type_.value
        }
    }
    if sortBy is not None:
        message['1'] = sortBy.value
    if requiresToSendPage(page):
        # I am not very comfortable with this hardcoded constant.
        message['9'] = page * 20

    typedef = {
        '2': {
            'field_order': [
                '2'
            ],
            'message_typedef': {
                '2': {
                    'type': 'int'
                }
            },
            'type': 'message'
        }
    }
    if sortBy is not None:
        typedef['1'] = {
            'type': 'int'
        }
    if requiresToSendPage(page):
        typedef['9'] = {
            'type': 'int'
        }

    params = getBase64Protobuf(message, typedef)

    data = {
        'query': query,
        'params': params,
    }

    data = getJSON('https://www.youtube.com/youtubei/v1/search', data)
    contents = data['contents']['twoColumnSearchResultsRenderer']['primaryContents']['sectionListRenderer']['contents']
    # Merging multiple array elements seems necessary, see [issues/58#issuecomment-2437450](https://codeberg.org/Benjamin_Loison/Webscrap_any_website/issues/58#issuecomment-2437450).
    items = []
    for content in contents:
        # To exclude `continuationItemRenderer`.
        if 'itemSectionRenderer' in content:
            items += content['itemSectionRenderer']['contents']
    return items

def getSearchedPlaylists(query, sortBy = None, page = None):
    playlists = search(query, Type.PLAYLIST, sortBy)
    def cleanVideo(video):
        childVideoRenderer = video['childVideoRenderer']
        return {
            'title': childVideoRenderer['title']['simpleText'],
            'id': childVideoRenderer['videoId'],
            'duration': getIntFromDuration(childVideoRenderer['lengthText']['simpleText']),
        }
    def cleanPlaylist(playlist):
        # Current design make an entry field path break the whole entry.
        # This was initially designed for next page token.
        try:
            playlistRenderer = playlist['playlistRenderer']
            browseEndpoint = playlistRenderer['longBylineText']['runs'][0]['navigationEndpoint']['browseEndpoint']
            return {
                'id': playlistRenderer['playlistId'],
                'title': playlistRenderer['title']['simpleText'],
                'video count': int(playlistRenderer['videoCount']),
                # Unclear why those videos.
                'thumbnails': [thumbnails['thumbnails'] for thumbnails in playlistRenderer['thumbnails']],
                'showcase video thumbnails': playlistRenderer['thumbnailRenderer']['playlistVideoThumbnailRenderer']['thumbnail']['thumbnails'],
                # Is there an issue if a playlist video is from a different channel? Not easy to verify as most channels advertize themselves.
                'channel handle': browseEndpoint['canonicalBaseUrl'][1:],
                'channel id': browseEndpoint['browseId'],
                'videos': list(map(cleanVideo, playlistRenderer['videos']))
            }
        except KeyError:
            return
    playlists = list(map(cleanPlaylist, playlists))
    if playlists[-1] is None:
        playlists.pop()
    return playlists

def getSearchedChannels(query, sortBy = None, page = None):
    channels = search(query, Type.CHANNEL, sortBy, page)
    def cleanChannel(channel):
        channelRenderer = channel['channelRenderer']
        return {
            'id': channelRenderer['channelId'],
            'title': channelRenderer['title']['simpleText'],
            'handle': channelRenderer['subscriberCountText']['simpleText'],
            'thumbnails': channelRenderer['thumbnail']['thumbnails'],
            # *bold* seems specific to the specified `query`
            'description': ''.join([run['text'] for run in channelRenderer['descriptionSnippet']['runs']]) if 'descriptionSnippet' in channelRenderer else None,
            'subscriber count': getIntValue(channelRenderer.get('videoCountText', channelRenderer['subscriberCountText'])['simpleText'], 'subscriber'),
            # I have not quickly found a channel with 2 approvals.
            # Same on YouTube operational API according to `grep -rw 'ownerBadges'`.
            'channel badges': channelRenderer['ownerBadges'][0]['metadataBadgeRenderer']['tooltip'] if 'ownerBadges' in channelRenderer else None,
        }
    channels = [cleanChannel(channel) for channel in channels if 'channelRenderer' in channel]
    return channels

def getSearchedVideos(query, sortBy = None, page = None):
    videos = search(query, Type.VIDEO, sortBy, page)
    def cleanVideo(video):
        videoRenderer = video['videoRenderer']
        ownerTextRun = videoRenderer['ownerText']['runs'][0]
        browseEndpoint = ownerTextRun['navigationEndpoint']['browseEndpoint']
        viewCountText = videoRenderer['viewCountText']
        return {
            'id': videoRenderer['videoId'],
            'thumbnails': videoRenderer['thumbnail']['thumbnails'],
            'title': videoRenderer['title']['runs'][0]['text'],
            'channel handle': browseEndpoint['canonicalBaseUrl'][1:],
            'channel id': browseEndpoint['browseId'],
            'channel name': ownerTextRun['text'],
            'channel thumbnail': videoRenderer['avatar']['decoratedAvatarViewModel']['avatar']['avatarViewModel']['image']['sources'][0],
            'description': ''.join([run['text'] for run in videoRenderer['detailedMetadataSnippets'][0]['snippetText']['runs']]) if 'detailedMetadataSnippets' in videoRenderer else None,
            'view count': getIntValue(viewCountText['simpleText'], 'view') if 'simpleText' in viewCountText else int(viewCountText['runs'][0]['text'].replace(',', '')),
            'duration': getIntFromDuration(videoRenderer['lengthText']['simpleText']) if 'lengthText' in videoRenderer else None,
            # *badges* *New* does not seem relevant compared to *published time*.
            'published time': videoRenderer['publishedTimeText']['simpleText'] if 'publishedTimeText' in videoRenderer else None,
        }
    videos = [cleanVideo(video) for video in videos if 'videoRenderer' in video]
    return videos