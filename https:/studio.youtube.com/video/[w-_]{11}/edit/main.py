import sys

sys.path.insert(0, '../../../../www.youtube.com')

from main import USER_AGENT, getSAPISIDHASH

import requests
from lxml import html
import re
import json

HEADERS = {
    'User-Agent': USER_AGENT,
}

'''
`cookies` must have:
- `__Secure-1PSIDTS`
- `__Secure-1PSID`
- `__Secure-1PAPISID` or `__Secure-3PAPISID`
'''
def getVideoDetails(videoId, cookies):
    text = requests.get(f'https://studio.youtube.com/video/{videoId}/edit', cookies = cookies, headers = HEADERS).text
    tree = html.fromstring(text)
    for script in tree.xpath('//script'):
        scriptContent = script.text_content()
        fullMatch = re.fullmatch("window\\.chunkedPrefetchResolvers\\['id\\-1'\\]\\.resolve\\((.*)\\);", scriptContent)
        if fullMatch is not None:
            fullMatchGroup = fullMatch.group(1)
            # Missing other fields.
            fullMatchGroupData = json.loads(fullMatchGroup)
            game = fullMatchGroupData['gameTitle']
            return {
                'game': {
                    'title': game['title'],
                    'm id': game['mid'],
                },
                'original file name': fullMatchGroupData['originalFilename'],
            }

'''
`cookies` must have:
- `__Secure-1PSIDTS`
- `__Secure-1PSID`
- `__Secure-1PAPISID`
'''
def setVideoGame(videoId, gameMId, cookies, sessionInfoToken):
    SAPISIDHASH = getSAPISIDHASH(cookies['__Secure-1PAPISID'])

    headers = {
        'Authorization': f'SAPISIDHASH {SAPISIDHASH}',
        'Origin': ORIGIN,
    }

    data = {
        'encryptedVideoId': videoId,
        'gameTitle': {
            'newKgEntityId': gameMId,
        },
        'context': {
            'client': {
                'clientName': 62,
                'clientVersion': '1.20250224.01.01',
            },
            'request': {
                'sessionInfo': {
                    'token': sessionInfoToken,
                },
            },
        },
    }

    response = requests.post(
        'https://studio.youtube.com/youtubei/v1/video_manager/metadata_update',
        cookies = cookies,
        headers = headers,
        json = data,
    )
    return response.json()['gameTitle']['success']
