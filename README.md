# Webscrap any website

## Install your own HTTP interface instance:

### Host a website:

See https://github.com/Benjamin-Loison/YouTube-operational-API/blob/88443c44f8de2d312e549c00647977a4358e94f7/README.md#install-your-own-instance-of-the-api.

### Clone the repository:

```bash
git clone https://codeberg.org/Benjamin_Loison/Webscrap_any_website
```

### Install dependencies:

```bash
pip install waitress flask requests bbpb lxml
```

### Start the HTTP interface:

```bash
waitress-serve --call http_interface:createApp
```

You can add a reverse proxy to your web server.
If you do so, you may be interested by blocking direct access with `--listen=localhost:8080`.
