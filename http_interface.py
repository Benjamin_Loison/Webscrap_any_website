from flask import Flask, Response, request
from werkzeug.routing import BaseConverter
import os
import json
import re

app = Flask(__name__)

class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]

app.url_map.converters['regex'] = RegexConverter

WEBPAGE_WEB_SCRAPING_SCRIPT_ROUTES = [
    'https:/www.youtube.com/channel/<regex("UC[\\w\\-_]{22}"):channelId>/videos',
    'https:/www.youtube.com/channel/<regex("UC[\\w\\-_]{22}"):channelId>/shorts',
    'https:/www.youtube.com/watch',
    'https:/www.youtube.com/results',
]

currentWorkingDirectory = os.getcwd()

for webpageWebScrapingScriptRoute in WEBPAGE_WEB_SCRAPING_SCRIPT_ROUTES:
    webpageWebScrapingScriptPath = re.sub('<regex\\("(.*)"\\):.*>', '\\1', webpageWebScrapingScriptRoute)
    webpageWebScrapingScriptPath = webpageWebScrapingScriptPath
    os.chdir(f'{currentWorkingDirectory}/{webpageWebScrapingScriptPath}')
    exec(open('main.py').read())

def getJsonResponseFromData(data):
    dataStr = json.dumps(data, indent = 4)
    return Response(dataStr, mimetype = 'application/json')

def getJsonResponse(function, arguments):
    return getJsonResponseFromData(function(*arguments))

def getError(errorMessage):
    return getJsonResponseFromData({'error': errorMessage})

def getFieldError(fieldName, fieldValue):
    return getError(f'Missing `{fieldName}`!' if fieldValue is None else f'Invalid `{fieldName}`!')

# Should use unique names like the URL to prevent collisions.
# See [Webscrap_any_website/issues/12#issuecomment-2407141](https://codeberg.org/Benjamin_Loison/Webscrap_any_website/issues/12#issuecomment-2407141).
@app.route(f'/{WEBPAGE_WEB_SCRAPING_SCRIPT_ROUTES[0]}')
def _popularVideos(channelId):
    part = request.form.get('part')
    match part:
        case 'popular':
            return getJsonResponse(getPopularVideos, [channelId])
        case _:
            return getFieldError('part', part)

@app.route(f'/{WEBPAGE_WEB_SCRAPING_SCRIPT_ROUTES[1]}')
def _popularShorts(channelId):
    part = request.form.get('part')
    match part:
        case 'popular':
            return getJsonResponse(getPopularShorts, [channelId])
        case _:
            return getFieldError('part', part)

@app.route(f'/{WEBPAGE_WEB_SCRAPING_SCRIPT_ROUTES[2]}')
def _commentCount():
    VIDEO_ID_ARGUMENT = 'v'
    videoId = request.args.get(VIDEO_ID_ARGUMENT)
    if videoId is None:
        return getError(f'Missing argument `{VIDEO_ID_ARGUMENT}`!')
    VIDEO_ID_REGEX = '[\\w\\-_]{11}'
    if re.fullmatch(VIDEO_ID_REGEX, videoId) is None:
        return getError(f'Argument `{VIDEO_ID_ARGUMENT}` does not match regex `{VIDEO_ID_REGEX}`!')
    part = request.form.get('part')
    match part:
        case 'comment count':
            return getJsonResponseFromData({'comment count': getCommentCount(videoId)})
        case _:
            return getFieldError('part', part)

@app.route(f'/{WEBPAGE_WEB_SCRAPING_SCRIPT_ROUTES[3]}')
def _search():
    SEARCH_QUERY_ARGUMENT = 'search_query'
    searchQuery = request.args.get(SEARCH_QUERY_ARGUMENT)
    if searchQuery is None:
        return getError(f'Missing argument `{SEARCH_QUERY_ARGUMENT}`!')
    # Do not know if any search query character is forbidden.
    type_ = request.form.get('type')
    sortBy = request.form.get('sort by')
    if sortBy is not None:
        sortByEnumCase = SortBy[sortBy.upper().replace(' ', '_')]
        sortBy = sortByEnumCase if sortByEnumCase.value is not None else None
    page = request.form.get('page')
    if page is not None:
        page = int(page)
    parameters = [searchQuery, sortBy, page]
    match type_:
        case 'channel':
            return getJsonResponse(getSearchedChannels, parameters)
        case 'video':
            return getJsonResponse(getSearchedVideos, parameters)
        case 'playlist':
            return getJsonResponse(getSearchedPlaylists, parameters)
        case _:
            return getFieldError('type', type_)

def createApp():
   return app
